#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "numberGame.h"

int choosePlayerNumber()
{
    int playerNumber = 0;

    do
    {
        printf("== == MENU == == \n");
        printf("1. MODE 1 JOUEUR \n");
        printf("2. MODE 2 JOUEUR \n");
        printf("Quel est votre choix ?\n");
        scanf("%d", &playerNumber);
        system("clear");

        switch (playerNumber)
        {
        case 1:
            printf("Vous avez choisi de jouer a %d joueur\n\n", playerNumber);
            break;
        case 2:
            printf("Vous avez choisi de jouer a %d joueurs\n\n", playerNumber);
            break;
        default:
            printf("Vous n avez pas choisi un nombre de joueur valable\n\n");
            break;
        }
    } while (playerNumber != 1 && playerNumber != 2);

    return playerNumber;
}

int playAgain()
{
    int continueToPlay = 0;
    do
    {
        printf("Veux-tu continuer a jouer ?\n");
        printf("1. oui\n");
        printf("2. non\n");

        scanf("%d", &continueToPlay);
        system("clear");

        switch (continueToPlay)
        {
        case 1:
            printf("Vous avez choisi de continuer a jouer\n\n");
            break;
        case 2:
            printf("Merci d avoir jouer \n");
            printf("A bientot\n\n");
            system("exit");
            break;
        default:
            printf("Votre choix n est pas valabe\n\n");
            break;
        }
    } while (continueToPlay != 1 && continueToPlay != 2);

    return (continueToPlay);
}

Dificulty chooseDificultyMode()
{
    Dificulty d;
    d.min = 1;

    do
    {
        printf("== == CHOIX DU NIVEAU DE DIFFICULTE == == \n");
        printf("1. Nombre entre 1 et 100 \n");
        printf("2. Nombre entre 1 et 1000 \n");
        printf("3. Nombre entre 1 et 10000 \n");
        printf("Quel est votre choix ?\n");

        scanf("%d", &d.difficultyMode);
        system("clear");

        switch (d.difficultyMode)
        {
        case 1:
            printf("Vous avez choisi le niveau de difficulte %d\n\n", d.difficultyMode);
            d.max = 100;
            break;

        case 2:
            printf("Vous avez choisi le niveau de difficulte %d\n\n", d.difficultyMode);
            d.max = 1000;
            break;

        case 3:
            printf("Vous avez choisi le niveau de difficulte %d\n\n", d.difficultyMode);
            d.max = 10000;
            break;

        default:
            printf("Vous n avez pas choisi un niveau de difficulte valide\n\n");
            break;
        }
    } while (d.difficultyMode != 1 && d.difficultyMode != 2 && d.difficultyMode != 3);

    return d;
}

Numbers findNumberOnePlayer(int max, int min)
{
    int roundCount = 1;
    Numbers n;

    srand(time(NULL));
    n.mysteryNumber = (rand() % (max - min + 1)) + min;

    printf("Essaye de deviner le nombre entre 1 et %d?\n\n", max);

    do
    {
        printf("Quel est le nombre ?\n");
        scanf("%d", &n.numberToCheck);

        if (n.numberToCheck == n.mysteryNumber)
        {
            printf("Bravo tu as trouve en %d\n\n", roundCount);
        }
        else if (1 <= n.numberToCheck && n.numberToCheck < n.mysteryNumber)
        {
            printf("Ton nombre est trop petit, essaye encore\n");
        }
        else if (max >= n.numberToCheck && n.numberToCheck > n.mysteryNumber)
        {
            printf("Ton nombre est trop grand, essaye encore\n");
        }
        else
        {
            printf("Tu n as pas tape un nombre entre 1 et %d, essaye encore\n", max);
        }
        roundCount++;

    } while (n.numberToCheck != n.mysteryNumber);

    return n;
}

Numbers findNumberTwoPlayer(int max, int min)
{
    Numbers n;
    int roundCount = 1;

    printf("Joueur 1 : choisissez une nombre entre 1 et %d\n", max);
    printf("Joueur 2 : Essaye de deviner le nombre entre 1 et %d?\n\n", max);

    do
    {
        printf("Joueur 1 : Entre ton nombre sans le montrer au joueur 2\n");
        scanf("%d", &n.mysteryNumber);

        if (n.mysteryNumber < 1 || n.mysteryNumber > max)
        {
            printf("Le nombre choisit n'est pas entre %d et %d\n\n", min, max);
        }
    }while (n.mysteryNumber < 1 || n.mysteryNumber > max);
    system("clear");
    
    do
    {
        printf("Joueur 2 : Quel est le nombre choisi par le joueur 1?\n");
        scanf("%d", &n.numberToCheck);

        if (n.numberToCheck == n.mysteryNumber)
        {
            printf("Bravo tu as trouve en %d\n\n", roundCount);
        }
        else if (1 <= n.numberToCheck && n.numberToCheck < n.mysteryNumber)
        {
            printf("Ton nombre est trop petit, essaye encore\n");
        }
        else if (max >= n.numberToCheck && n.numberToCheck > n.mysteryNumber)
        {
            printf("Ton nombre est trop grand, essaye encore\n");
        }
        else
        {
            printf("Tu n as pas tape un nombre entre 1 et %d, essaye encore\n", max);
        }
        roundCount++;

    } while (n.numberToCheck != n.mysteryNumber);

    return n;
}

int main()
{

    int continueToPlay = 1;
    while (continueToPlay == 1)
    {
        int numberToCheck = 0;
        int mysteryNumber = 0;
        int playerNumber = choosePlayerNumber();
        Dificulty d = chooseDificultyMode();
        switch (playerNumber)
        {
        case 1:
            findNumberOnePlayer(d.max, d.min);
            break;
        case 2:
            findNumberTwoPlayer(d.max, d.min);
        }
        continueToPlay = playAgain();
    }
    return 0;
}