#ifndef DEF_NUMBERGAME
#define DEF_NUMBERGAME

int choosePlayerNumber();
int playAgain();

struct Dificulty
{
    int max;
    int min;
    int difficultyMode;
};

typedef struct Dificulty Dificulty;

struct Numbers
{
    int numberToCheck;
    int mysteryNumber;
};

typedef struct Numbers Numbers;

Dificulty chooseDificultyMode();
Numbers findNumberOnePlayer(int max, int min);
Numbers findNumberTwoPlayer(int max, int min);

#endif